module.exports = app => {
    const customers = require("../controllers/customer.controller.js");
  
    var router = require("express").Router();
  
    // Retrieve all Customers
    router.get("/", customers.findAndCountAll);
    app.use('/api/customers', router);
  };